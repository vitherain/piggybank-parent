package cz.herain.piggybank.common.repository;

import cz.herain.piggybank.common.entity.Income;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.List;

public interface IncomeRepository extends BaseRepository<Income> {

    @Query("SELECT i FROM Income i WHERE i.costMonth.id = :monthId ORDER BY i.dayInMonth ASC")
    List<Income> findByMonthIdSortedByDayDesc(@Param("monthId") long monthId);

    @Query(
            value = "SELECT sum(amount) FROM income WHERE cost_month_id = :monthId",
            nativeQuery = true
    )
    BigInteger countSumAmountByCostMonthId(@Param("monthId") long monthId);

    @Query(
            value =
                    "SELECT COALESCE(SUM(i.amount), 0) / :costMonthsCount " +
                            "FROM (" +
                            "SELECT * FROM income WHERE (cost_month_id in (:costMonthIds))" +
                            ") i " +
                            "RIGHT OUTER JOIN purpose p ON i.purpose_id = p.id " +
                            "WHERE p.user_id = :userId " +
                            "GROUP BY p.name " +
                            "ORDER BY p.name",

            nativeQuery = true
    )
    List<BigInteger> averagesByPurposeAndCostMonthsSortedByPurposeName(
            @Param("costMonthIds") List<Long> costMonthIds,
            @Param("costMonthsCount") int costMonthsCount,
            @Param("userId") long userId);

    @Query(
            value =
                    "SELECT COALESCE(SUM(i.amount), 0) " +
                            "FROM income i " +
                            "WHERE i.purpose_id = :purposeId AND i.cost_month_id = :monthId",
            nativeQuery = true
    )
    BigInteger countSumByPurposeIdAndMonthId(@Param("purposeId") Long purposeId, @Param("monthId") long monthId);

    @Query(
            value =
                    "SELECT COALESCE(SUM(i.amount), 0) " +
                            "FROM income i " +
                            "WHERE i.purpose_id = :purposeId AND i.cost_month_id in :costMonthIds",
            nativeQuery = true
    )
    BigInteger countSumByPurposeIdAndMonthIds(@Param("purposeId") long purposeId, @Param("costMonthIds") List<Long> costMonthIds);

    @Query(
            value = "SELECT sum(amount) FROM income WHERE cost_month_id in :costMonthIds",
            nativeQuery = true
    )
    BigInteger countSumAmountByCostMonthIds(@Param("costMonthIds") List<Long> costMonthIds);
}
