angular.module('piggy-bank').controller("CashFlowController", ["$scope", "$q", "httpFacade", "toasty", "arrayUtils", "modalService",
    function($scope, $q, httpFacade, toasty, arrayUtils, modalService) {

    function reloadUsersCostPeriods() {
        httpFacade.getResource("usersCostPeriods").then(function(response) {
            $scope.costYears = response.data;
        });
    }
    reloadUsersCostPeriods();

    $scope.selectYear = function(yearId) {
        $scope.selectedMonthId = null;
        $scope.expenses = null;
        $scope.incomes = null;

        var year = arrayUtils.getObjectFromArray($scope.costYears, function(year) {
            return year.id === yearId;
        });
        $scope.selectedYearId = yearId;
        $scope.costMonths = year.costMonths;
    };

    $scope.addYear = function() {
        var knownYears = arrayUtils.getSubArrayByElementsPropertyName($scope.costYears, 'year');
        var modal = modalService.openAddYearDialog({}, knownYears);

        modal.result
            .then(submitYear)
            .catch(function(reason) {
                if (!modalService.isCancelReason(reason)) {
                    toasty.unexpectedError();
                }
            });
    };

    function submitYear(year) {
        httpFacade.postResource("submitCostYear", year)
            .then(function() {
                resetData();
                reloadUsersCostPeriods();
            });
    }

    $scope.selectMonth = function(monthId) {
        $scope.selectedMonthId = monthId;
        reloadData();
    };

    $scope.addMonth = function() {
        var knownMonths = arrayUtils.getSubArrayByElementsPropertyName($scope.costMonths, 'monthInYear');
        var modal = modalService.openAddMonthDialog({costYearId: $scope.selectedYearId}, knownMonths);

        modal.result
            .then(submitMonth)
            .catch(function(reason) {
                if (!modalService.isCancelReason(reason)) {
                    toasty.unexpectedError();
                }
            });
    };

    function submitMonth(month) {
        httpFacade.postResource("submitCostMonth", month)
            .then(function() {
                resetData();
                reloadUsersCostPeriods();
            });
    }

    $scope.editExpense = function(expense) {
        var modal = modalService.openEditExpenseDialog(angular.copy(expense), $scope.purposes);

        modal.result
            .then(submitExpense)
            .catch(function(reason) {
                if (!modalService.isCancelReason(reason)) {
                    toasty.unexpectedError();
                }
            });
    };

    function submitExpense(expense) {
        httpFacade.postResource("submitExpense", expense)
            .then(function() {
                reloadData();
            });

        var defaultPurpose = arrayUtils.getObjectFromArray($scope.purposes, function(item) {
            return item.defaultForCurrentUser === true;
        });
        var defaultPurposeId;
        if (defaultPurpose.hasOwnProperty('id')) {
            defaultPurposeId = defaultPurpose.id;
        }
        $scope.newExpense = { costMonthId: $scope.selectedMonthId, purposeId: defaultPurposeId };
    }

    $scope.editIncome = function(income) {
        var modal = modalService.openEditIncomeDialog(angular.copy(income), $scope.purposes);

        modal.result
            .then(submitIncome)
            .catch(function(reason) {
                if (!modalService.isCancelReason(reason)) {
                    toasty.unexpectedError();
                }
            });
    };

    function submitIncome(income) {
        httpFacade.postResource("submitIncome", income)
            .then(function () {
                reloadData();
            });

        var defaultPurpose = arrayUtils.getObjectFromArray($scope.purposes, function(item) {
            return item.defaultForCurrentUser === true;
        });
        var defaultPurposeId;
        if (defaultPurpose.hasOwnProperty('id')) {
            defaultPurposeId = defaultPurpose.id;
        }
        $scope.newIncome = { costMonthId: $scope.selectedMonthId, purposeId: defaultPurposeId };
    }

    function reloadData() {
        resetData();
        reloadPurposes().then(function() {
            var defaultPurpose = arrayUtils.getObjectFromArray($scope.purposes, function(item) {
                return item.defaultForCurrentUser === true;
            });
            var defaultPurposeId;
            if (defaultPurpose.hasOwnProperty('id')) {
                defaultPurposeId = defaultPurpose.id;
            }

            $scope.newExpense = { costMonthId: $scope.selectedMonthId, purposeId: defaultPurposeId };
            $scope.newIncome = { costMonthId: $scope.selectedMonthId, purposeId: defaultPurposeId };
        });
        reloadMonthData();
    }

    function resetData() {
        $scope.balance = null;
        $scope.sumOfExpenses = null;
        $scope.sumOfIncomes = null;
        $scope.expenses = null;
        $scope.incomes = null;
    }

    function reloadPurposes() {
        return httpFacade.getResource("usersPurposes").then(function(response) {
            $scope.purposes = response.data;
            return response.data;
        });
    }

    function reloadMonthData() {
        return httpFacade.getResource("usersMonthSummary", {monthId: $scope.selectedMonthId}).then(function(response) {
            var monthData = response.data;
            $scope.expenses = monthData.expenses;
            $scope.sumOfExpenses = monthData.sumOfExpenses;
            $scope.incomes = monthData.incomes;
            $scope.sumOfIncomes = monthData.sumOfIncomes;
            $scope.balance = monthData.balance;

            return monthData;
        });
    }

    $scope.deleteExpense = function(expense) {
        httpFacade.postResource("deleteExpense", { expenseId: expense.id })
            .then(function() {
                reloadData();
            })
    };

    $scope.deleteIncome = function(income) {
        httpFacade.postResource("deleteIncome", { incomeId: income.id })
            .then(function() {
                reloadData();
            })
    };
}]);