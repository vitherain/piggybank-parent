package cz.herain.piggybank.common.entity;

import java.io.Serializable;

public interface IdentifiableObject extends Serializable {

    Long getId();

    void setId(Long id);
}
