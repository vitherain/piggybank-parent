package cz.herain.piggybank.common.entity;

import javax.persistence.*;

@Entity
@Table(name = "income")
public class Income extends BaseEntity {

    @Column(name = "day_in_month", nullable = false)
    private int dayInMonth;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "cost_month_id", nullable = false)
    private CostMonth costMonth;

    @ManyToOne(optional = true)
    @JoinColumn(name = "purpose_id", nullable = true)
    private Purpose purpose;

    @Column(nullable = true)
    private int amount;

    @Column(nullable = true)
    private String note;

    @Column(nullable = true)
    private String originator;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public CostMonth getCostMonth() {
        return costMonth;
    }

    public void setCostMonth(CostMonth costMonth) {
        this.costMonth = costMonth;
    }

    public int getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(int dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Income income = (Income) o;

        if (dayInMonth != income.dayInMonth) return false;
        if (amount != income.amount) return false;
        if (costMonth != null ? !costMonth.equals(income.costMonth) : income.costMonth != null) return false;
        if (purpose != null ? !purpose.equals(income.purpose) : income.purpose != null) return false;
        if (note != null ? !note.equals(income.note) : income.note != null) return false;
        return originator != null ? originator.equals(income.originator) : income.originator == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + dayInMonth;
        result = 31 * result + (costMonth != null ? costMonth.hashCode() : 0);
        result = 31 * result + (purpose != null ? purpose.hashCode() : 0);
        result = 31 * result + amount;
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (originator != null ? originator.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Income{" +
                "id=" + id +
                ", amount=" + amount +
                ", dayInMonth=" + dayInMonth +
                ", costMonth=" + costMonth.getMonthInYear() +
                ", purposeId=" + (purpose != null ? purpose.getId() : null) +
                ", note='" + note + '\'' +
                ", originator='" + originator + '\'' +
                '}';
    }
}
