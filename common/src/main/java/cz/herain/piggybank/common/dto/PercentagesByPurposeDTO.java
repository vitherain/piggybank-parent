package cz.herain.piggybank.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Vit Herain on 06/11/2016.
 */
public class PercentagesByPurposeDTO implements Serializable {

    private String purposeName;
    private BigDecimal percentage;

    public String getPurposeName() {
        return purposeName;
    }

    public void setPurposeName(final String purposeName) {
        this.purposeName = purposeName;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(final BigDecimal percentage) {
        this.percentage = percentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PercentagesByPurposeDTO that = (PercentagesByPurposeDTO) o;

        if (purposeName != null ? !purposeName.equals(that.purposeName) : that.purposeName != null) return false;
        return percentage != null ? percentage.equals(that.percentage) : that.percentage == null;

    }

    @Override
    public int hashCode() {
        int result = purposeName != null ? purposeName.hashCode() : 0;
        result = 31 * result + (percentage != null ? percentage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PercentagesByPurposeDTO{" +
                "purposeName='" + purposeName + '\'' +
                ", percentage=" + percentage +
                '}';
    }

    public static class PercentagesByPurposeDTOBuilder {
        private BigDecimal percentage;
        private String purposeName;

        private PercentagesByPurposeDTOBuilder() {
        }

        public static PercentagesByPurposeDTOBuilder aPercentagesByPurposeDTO() {
            return new PercentagesByPurposeDTOBuilder();
        }

        public PercentagesByPurposeDTOBuilder withPercentage(BigDecimal percentage) {
            this.percentage = percentage;
            return this;
        }

        public PercentagesByPurposeDTOBuilder withPurposeName(String purposeName) {
            this.purposeName = purposeName;
            return this;
        }

        public PercentagesByPurposeDTOBuilder but() {
            return aPercentagesByPurposeDTO().withPercentage(percentage).withPurposeName(purposeName);
        }

        public PercentagesByPurposeDTO build() {
            PercentagesByPurposeDTO percentagesByPurposeDTO = new PercentagesByPurposeDTO();
            percentagesByPurposeDTO.setPercentage(percentage);
            percentagesByPurposeDTO.setPurposeName(purposeName);
            return percentagesByPurposeDTO;
        }
    }
}
