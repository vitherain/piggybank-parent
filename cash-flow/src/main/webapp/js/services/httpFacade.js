angular.module('piggy-bank').service("httpFacade", ["$q", "$http", "$rootScope", function($q, $http, $rootScope) {

    this.getResourceUrlAsync = function() {
        var deferred = $q.defer();

        AUI().ready('liferay-portlet-url', function() {
            var resourceUrl = Liferay.PortletURL.createResourceURL();
            resourceUrl.setPortletId(cashFlowPortletNamespaceTrimmed);

            deferred.resolve(resourceUrl);
        });

        return deferred.promise;
    };

    this.getActionUrlAsync = function() {
        var deferred = $q.defer();

        AUI().ready('liferay-portlet-url', function() {
            var actionUrl = Liferay.PortletURL.createActionURL();
            actionUrl.reservedParams.p_auth = Liferay.authToken;
            actionUrl.setPortletId(cashFlowPortletNamespaceTrimmed);
            actionUrl.setWindowState(cashFlowWindowState);

            deferred.resolve(actionUrl);
        });

        return deferred.promise;
    };

    this.getResource = function(resourceId, parameters) {
        return this.getResourceUrlAsync().then(function(resourceUrl) {
            resourceUrl.setResourceId(resourceId);

            for (var param in parameters) {
                if(!parameters.hasOwnProperty(param)) continue;

                resourceUrl.setParameter(param, parameters[param]);
            }

            return $http({
                method: 'GET',
                url: resourceUrl.toString()
            });
        });
    };

    this.postResource = function(resourceId, data, parameters) {
        return this.getResourceUrlAsync().then(function(resourceUrl) {
            resourceUrl.setResourceId(resourceId);

            for (var param in parameters) {
                if(!parameters.hasOwnProperty(param)) continue;

                resourceUrl.setParameter(param, parameters[param]);
            }

            return $http({
                method: 'POST',
                url: resourceUrl.toString(),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(data) {
                    var result = '';
                    for (var prop in data) {
                        if(!data.hasOwnProperty(prop)) continue;

                        // request body params delimiter
                        if (result) {
                            result = result + '&'
                        }

                        result = result + cashFlowPortletNamespace + prop + '=' + data[prop];
                    }
                    return result;
                },
                data: data
            });
        });
    };

    this.getAction = function(actionName, parameters) {
        return this.getActionUrlAsync().then(function(actionUrl) {
            actionUrl.setName(actionName);

            for (var param in parameters) {
                if(!parameters.hasOwnProperty(param)) continue;

                actionUrl.setParameter(param, parameters[param]);
            }

            return $http({
                method: 'GET',
                url: actionUrl.toString()
            });
        });
    };

    this.postAction = function(actionName, data, parameters) {
        return this.getActionUrlAsync().then(function(actionUrl) {
            actionUrl.setName(actionName);

            for (var param in parameters) {
                if(!parameters.hasOwnProperty(param)) continue;

                actionUrl.setParameter(param, parameters[param]);
            }

            return $http({
                method: 'POST',
                url: actionUrl.toString(),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(data) {
                    var result = '';
                    for (var prop in data) {
                        if(!data.hasOwnProperty(prop)) continue;

                        // request body params delimiter
                        if (result) {
                            result = result + '&'
                        }

                        result = result + cashFlowPortletNamespace + prop + '=' + data[prop];
                    }
                    return result;
                },
                data: data
            });
        });
    };
}]);