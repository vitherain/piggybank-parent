angular.module('piggy-bank-analytics').factory("SumsByPurposeDTO", ["AmountDTO", function(AmountDTO) {

    class SumsByPurposeDTO {

        constructor(arg) {
            this.id = arg.id ? arg.id : undefined;
            this.purposeName = arg.purposeName ? arg.purposeName : undefined;
            this.show = true;
            this.expensesAmount = arg.expensesAmount ? new AmountDTO(arg.expensesAmount) : undefined;
            this.incomesAmount = arg.incomesAmount ? new AmountDTO(arg.incomesAmount) : undefined;
        }
    }

    return SumsByPurposeDTO;
}]);