package cz.herain.piggybank.cashflow.controller;

import com.google.gson.Gson;
import cz.herain.piggybank.common.controller.PortletControllerAdvice;
import cz.herain.piggybank.common.dto.CostMonthCompleteDTO;
import cz.herain.piggybank.common.dto.ExpenseDTO;
import cz.herain.piggybank.common.dto.IncomeDTO;
import cz.herain.piggybank.common.dto.PurposeDTO;
import cz.herain.piggybank.common.exception.ValidationException;
import cz.herain.piggybank.common.service.CostMonthService;
import cz.herain.piggybank.common.service.ExpenseService;
import cz.herain.piggybank.common.service.IncomeService;
import cz.herain.piggybank.common.service.PurposeService;
import cz.herain.piggybank.common.util.ContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class CashFlowController extends PortletControllerAdvice {

    @Autowired
    private PurposeService purposeService;

    @Autowired
    private CostMonthService costMonthService;

    @Autowired
    private ExpenseService expenseService;

    @Autowired
    private IncomeService incomeService;

    @Autowired
    private ContextUtil contextUtil;

    @ResourceMapping("usersPurposes")
    public void getUsersPurposes(ResourceRequest request, ResourceResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();

        long userId = contextUtil.getCurrentPbUserId();
        List<PurposeDTO> purposes = purposeService.getActiveByUserIdSortedByNameAsc(userId);
        String purposesJson = gson.toJson(purposes);

        out.print(purposesJson);
    }

    @ResourceMapping("usersMonthSummary")
    public void getUsersMonthBalance(@RequestParam(value = "monthId") long monthId,
                        ResourceRequest request,
                        ResourceResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();

        CostMonthCompleteDTO monthSummary = costMonthService.getCompleteDtoById(monthId);
        String monthSummaryJson = gson.toJson(monthSummary);

        out.print(monthSummaryJson);
    }

    @ResourceMapping(value = "submitExpense")
    public void submitExpense(@ModelAttribute(value = "expense") @Valid ExpenseDTO expenseDto, BindingResult result,
                              ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
        if (result.hasErrors()) {
            throw new ValidationException(result);
        }

        expenseService.saveOrUpdate(expenseDto);
    }

    @ResourceMapping(value = "deleteExpense")
    public void deleteExpense(@RequestParam(value = "expenseId") long expenseId,
                              ResourceRequest resourceRequest, ResourceResponse resourceResponse) {

        expenseService.delete(expenseId);
    }

    @ResourceMapping(value = "submitIncome")
    public void submitIncome(@ModelAttribute(value = "income") @Valid IncomeDTO incomeDto, BindingResult result,
                              ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
        if (result.hasErrors()) {
            throw new ValidationException(result);
        }

        incomeService.saveOrUpdate(incomeDto);
    }

    @ResourceMapping(value = "deleteIncome")
    public void deleteIncome(@RequestParam(value = "incomeId") long incomeId,
                              ResourceRequest resourceRequest, ResourceResponse resourceResponse) {

        incomeService.delete(incomeId);
    }
}
