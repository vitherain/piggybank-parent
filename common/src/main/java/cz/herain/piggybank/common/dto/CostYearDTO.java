package cz.herain.piggybank.common.dto;

public class CostYearDTO extends BaseDTO {

    private int year;

    private Long pbUserId;

    public Long getPbUserId() {
        return pbUserId;
    }

    public void setPbUserId(Long pbUserId) {
        this.pbUserId = pbUserId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CostYearDTO that = (CostYearDTO) o;

        if (year != that.year) return false;
        return pbUserId != null ? pbUserId.equals(that.pbUserId) : that.pbUserId == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + year;
        result = 31 * result + (pbUserId != null ? pbUserId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CostYearDTO{" +
                "id=" + id +
                ", pbUserId=" + pbUserId +
                ", year=" + year +
                '}';
    }
}
