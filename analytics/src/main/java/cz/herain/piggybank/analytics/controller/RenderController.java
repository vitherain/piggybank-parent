package cz.herain.piggybank.analytics.controller;

import cz.herain.piggybank.analytics.util.Views;
import cz.herain.piggybank.common.controller.PortletControllerAdvice;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping("VIEW")
public class RenderController extends PortletControllerAdvice {

	@RenderMapping
	public String analyticsView() {
		return Views.ANALYTICS;
	}

    @RenderMapping(params = "render=balancesChart")
    public String balancesChart() {
        return Views.BALANCES_CHART;
    }

    @RenderMapping(params = "render=sumsByPurposeChart")
    public String sumsByPurposeChart() {
        return Views.SUMS_BY_PURPOSE_CHART;
    }

    @RenderMapping(params = "render=averageByPurposeChart")
    public String averageByPurposeChart() {
        return Views.AVERAGE_BY_PURPOSE_CHART;
    }

    @RenderMapping(params = "render=monthCashFlowChart")
    public String monthCashFlowChart() {
        return Views.MONTH_CASH_FLOW_CHART;
    }

    @RenderMapping(params = "render=percentagesByPurposeChart")
    public String percentagesByPurposeChart() {
        return Views.PERCENTAGES_BY_PURPOSE_CHART;
    }

    @RenderMapping(params = "render=averagePercentagesByPurposeChart")
    public String averagePercentagesByPurposeChart() {
        return Views.AVERAGE_PERCENTAGES_BY_PURPOSE_CHART;
    }
}