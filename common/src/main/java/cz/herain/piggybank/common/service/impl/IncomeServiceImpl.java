package cz.herain.piggybank.common.service.impl;

import cz.herain.piggybank.common.dto.AmountDTO;
import cz.herain.piggybank.common.dto.IncomeDTO;
import cz.herain.piggybank.common.entity.Income;
import cz.herain.piggybank.common.repository.IncomeRepository;
import cz.herain.piggybank.common.service.IncomeService;
import cz.herain.piggybank.common.service.base.impl.AbstractCommonService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Transactional(propagation = Propagation.REQUIRED)
@Service
public class IncomeServiceImpl
        extends AbstractCommonService<IncomeDTO, IncomeDTO, IncomeDTO, IncomeDTO, Income>
        implements IncomeService {

    private static final String ZERO_STRING = "0";

    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    private MapperFacade mapper;

    public IncomeServiceImpl() {
        super(IncomeDTO.class, IncomeDTO.class, IncomeDTO.class, IncomeDTO.class, Income.class);
    }

    @Override
    public List<IncomeDTO> getByMonthIdOrderedByDayAsc(long monthId) {
        List<Income> expenses = incomeRepository.findByMonthIdSortedByDayDesc(monthId);
        return mapper.mapAsList(expenses, IncomeDTO.class);
    }

    @Override
    public AmountDTO getSumOfIncomesByMonthId(long monthId) {
        BigInteger amount = incomeRepository.countSumAmountByCostMonthId(monthId);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public List<AmountDTO> getAveragesByPurposeAndCostMonthsOrderedByPurposeName(
            final List<Long> costMonthIds,
            final long userId) {

        final List<BigInteger> averages = incomeRepository
                .averagesByPurposeAndCostMonthsSortedByPurposeName(costMonthIds, costMonthIds.size(), userId);

        return averages.stream()
                .map((avg) -> AmountDTO.AmountDTOBuilder.anAmountDTO()
                        .withAmount(avg)
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public AmountDTO getSumOfIncomesByPurposeIdAndMonthId(final long purposeId, final long monthId) {
        BigInteger amount = incomeRepository.countSumByPurposeIdAndMonthId(purposeId, monthId);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public AmountDTO getSumOfIncomesByPurposeIdAndMonthIds(long purposeId, List<Long> costMonthIds) {
        BigInteger amount = incomeRepository.countSumByPurposeIdAndMonthIds(purposeId, costMonthIds);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public AmountDTO getSumOfIncomesByMonthIds(final List<Long> costMonthIds) {
        BigInteger amount = incomeRepository.countSumAmountByCostMonthIds(costMonthIds);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }
}
