package cz.herain.piggybank.common.repository;

import cz.herain.piggybank.common.entity.PbUser;

public interface PbUserRepository extends BaseRepository<PbUser> {

    PbUser findByUserId(long userId);
}
