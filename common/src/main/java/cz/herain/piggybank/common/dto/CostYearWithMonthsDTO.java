package cz.herain.piggybank.common.dto;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

public class CostYearWithMonthsDTO extends BaseDTO {

    private int year;

    private List<CostMonthDTO> costMonths;

    public List<CostMonthDTO> getCostMonths() {
        return costMonths;
    }

    public void setCostMonths(List<CostMonthDTO> costMonths) {
        this.costMonths = costMonths;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CostYearWithMonthsDTO that = (CostYearWithMonthsDTO) o;

        if (year != that.year) return false;
        return costMonths != null ? costMonths.equals(that.costMonths) : that.costMonths == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + year;
        result = 31 * result + (costMonths != null ? costMonths.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CostYearWithMonthsDTO{" +
                "id=" + id +
                ", costMonthsCount=" + (CollectionUtils.isNotEmpty(costMonths) ? costMonths.size() : 0) +
                ", year=" + year +
                '}';
    }
}
