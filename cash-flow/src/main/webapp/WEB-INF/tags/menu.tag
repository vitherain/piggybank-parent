<%@ tag description="Analytics portlet main menu" pageEncoding="UTF-8" %>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="col-xs-12">
	<c:if test="${pbUser != null}">
		<button class="btn btn-info" type="button" ng-click="addYear()" ng-disabled="!costYears">
			<span class="glyphicon glyphicon-time"></span> Add year
		</button>

		<button class="btn btn-primary" type="button" ng-click="addMonth()" ng-disabled="!selectedYearId">
			<span class="glyphicon glyphicon-calendar"></span> Add month
		</button>

		<button class="btn btn-danger" type="button" ng-click="editExpense(newExpense)" ng-disabled="!selectedMonthId">
			<span class="glyphicon glyphicon-thumbs-down"></span> Add expense
		</button>

		<button class="btn btn-success" type="button" ng-click="editIncome(newIncome)" ng-disabled="!selectedMonthId">
			<span class="glyphicon glyphicon-hand-right"></span> Add income
		</button>

		<liferay-portlet:renderURL var="defineCodeTableLink">
			<portlet:param name="render" value="definePurposeCodeTable"/>
		</liferay-portlet:renderURL>
		<a href="${defineCodeTableLink}" class="btn btn-warning">
			<span class="glyphicon glyphicon-th-list"></span> Define purposes
		</a>
	</c:if>

	<c:if test="${pbUser == null}">
		<button class="btn btn-info" type="button" disabled>
			<span class="glyphicon glyphicon-time"></span> Add year
		</button>

		<button class="btn btn-primary" type="button" disabled>
			<span class="glyphicon glyphicon-calendar"></span> Add month
		</button>

		<button class="btn btn-danger" type="button" disabled>
			<span class="glyphicon glyphicon-thumbs-down"></span> Add expense
		</button>

		<button class="btn btn-success" type="button" disabled>
			<span class="glyphicon glyphicon-hand-right"></span> Add income
		</button>

		<button type="button" class="btn btn-warning" disabled>
			<span class="glyphicon glyphicon-th-list"></span> Define purposes
		</button>
	</c:if>
</div>