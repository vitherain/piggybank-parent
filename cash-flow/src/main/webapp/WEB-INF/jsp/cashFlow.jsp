<%@include file="includes/header.jsp" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tag" %>

    <div class="container-fluid" ng-controller="CashFlowController">
        <%--MENU--%>
        <tag:menu/>
        <%--END MENU--%>

        <c:if test="${pbUser != null}">
            <span class="col-xs-12"><strong>Select year: </strong></span>
            <uib-tabset justified="true">
                <uib-tab ng-repeat="year in costYears" index="$index" ng-click="selectYear(year.id)">
                    <uib-tab-heading>{{ year.year }}</uib-tab-heading>

                        <%-- Nested tabs for months --%>
                    <span class="col-xs-12"><strong ng-if="selectedYearId && costMonths && costMonths.length">Select month: </strong></span>
                    <uib-tabset justified="true">
                        <uib-tab ng-repeat="month in costMonths" index="$index" ng-click="selectMonth(month.id)">
                            <uib-tab-heading>{{ month.monthInYear }}/{{ month.costYearAsNumber }}</uib-tab-heading>
                            <h3 ng-if="selectedMonthId">Balance:
                             <span ng-class="{ 'green-background': balance > 0, 'red-background': balance < 0, 'blue-background': balance === 0 }">
                            {{ balance }} CZK
                             </span>
                            </h3>

                            <div class="col-xs-6 float-left">
                                <h4 ng-if="expenses && expenses.length">Expenses</h4>
                                <span class="col-xs-3"><strong ng-if="selectedMonthId">Sum:</strong><span ng-if="selectedMonthId"> {{ sumOfExpenses }} CZK</span></span>

                                <table id="<portlet:namespace/>tabTemplateExpenses" class="table table-striped table-hover inline-table scroll-overflow" ng-if="expenses && expenses.length">
                                    <thead>
                                    <td class="col-xs-2"><strong>Day</strong></td>
                                    <td class="col-xs-2"><strong>Purpose</strong></td>
                                    <td class="col-xs-2"><strong>Recipient</strong></td>
                                    <td class="col-xs-2"><strong>Amount [CZK]</strong></td>
                                    <td class="col-xs-2"><strong>Note</strong></td>
                                    <td class="col-xs-2"><strong>Operations</strong></td>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="exp in expenses">
                                        <td>{{ exp.dayInMonth }}</td>
                                        <td>{{ exp.purposeName }}</td>
                                        <td>{{ exp.recipient }}</td>
                                        <td>{{ exp.amount }} CZK</td>
                                        <td>{{ exp.note }}</td>
                                        <td>
                                            <button class="btn-md btn-info" type="button" ng-click="editExpense(exp)">
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </button>
                                            <button class="btn-md btn-danger" type="button" ng-click="deleteExpense(exp)">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="align-center">
                                            <button class="btn btn-danger" type="button" ng-click="editExpense(newExpense)">
                                                <span class="glyphicon glyphicon-thumbs-down"></span> Add new
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-6 float-right">
                                <h4 ng-if="incomes && incomes.length">Incomes</h4>
                                <span class="col-xs-3"><strong ng-if="selectedMonthId">Sum:</strong><span ng-if="selectedMonthId"> {{ sumOfIncomes }} CZK</span></span>

                                <table id="<portlet:namespace/>tabTemplateIncomes" class="table table-striped table-hover inline-table scroll-overflow" ng-if="incomes && incomes.length">
                                    <thead>
                                    <td class="col-xs-2"><strong>Day</strong></td>
                                    <td class="col-xs-2"><strong>Purpose</strong></td>
                                    <td class="col-xs-2"><strong>Originator</strong></td>
                                    <td class="col-xs-2"><strong>Amount [CZK]</strong></td>
                                    <td class="col-xs-2"><strong>Note</strong></td>
                                    <td class="col-xs-2"><strong>Operations</strong></td>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="inc in incomes">
                                        <td>{{ inc.dayInMonth }}</td>
                                        <td>{{ inc.purposeName }}</td>
                                        <td>{{ inc.originator }}</td>
                                        <td>{{ inc.amount }} CZK</td>
                                        <td>{{ inc.note }}</td>
                                        <td>
                                            <button class="btn-md btn-info" type="button" ng-click="editIncome(inc)">
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </button>
                                            <button class="btn-md btn-danger" type="button" ng-click="deleteIncome(inc)">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="align-center">
                                            <button class="btn btn-success" type="button" ng-click="editIncome(newIncome)">
                                                <span class="glyphicon glyphicon-hand-right"></span> Add new
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </uib-tab>
                    </uib-tabset>
                </uib-tab>
            </uib-tabset>
        </c:if>

        <c:if test="${pbUser == null}">
            <h1>Please login to portal</h1>
        </c:if>
    </div>

<%@include file="includes/footer.jsp" %>