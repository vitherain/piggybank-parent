<toasty></toasty>
</div>

<script>
    var analyticsContextPath = "${pageContext.request.contextPath}";
    var analyticsPortletNamespace = "<portlet:namespace />";
    var analyticsWindowState = "${liferayPortletRequest.windowState.toString()}";

    //deletes unnecessary dashes
    var analyticsPortletNamespaceTrimmed = analyticsPortletNamespace.substring(1, analyticsPortletNamespace.length - 1);
</script>
<script src="${pageContext.request.contextPath}/js/app.js"></script>
<script src="${pageContext.request.contextPath}/js/interceptors/errorHandlingHttpInterceptor.js"></script>
<script src="${pageContext.request.contextPath}/js/services/httpFacade.js"></script>
<script src="${pageContext.request.contextPath}/js/services/arrayUtils.js"></script>
<script src="${pageContext.request.contextPath}/js/dtos/AmountDTO.js"></script>
<script src="${pageContext.request.contextPath}/js/dtos/SumsByPurposeDTO.js"></script>
<script src="${pageContext.request.contextPath}/js/controllers/AverageByPurposeChartController.js"></script>
<script src="${pageContext.request.contextPath}/js/controllers/AveragePercentagesByPurposeChartController.js"></script>
<script src="${pageContext.request.contextPath}/js/controllers/BalancesChartController.js"></script>
<script src="${pageContext.request.contextPath}/js/dtos/DayBalanceDTO.js"></script>
<script src="${pageContext.request.contextPath}/js/controllers/MonthCashFlowChartController.js"></script>
<script src="${pageContext.request.contextPath}/js/controllers/PercentagesByPurposeChartController.js"></script>
<script src="${pageContext.request.contextPath}/js/controllers/SumsByPurposeChartController.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrapApp.js"></script>