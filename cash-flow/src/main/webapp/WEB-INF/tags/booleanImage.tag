<%@tag description="Views green image for true value, red image for false value" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="value" required="true" %>

<c:choose>
    <c:when test="${value}">
        <img src="${pageContext.request.contextPath}/img/true.gif" alt="True"/>
    </c:when>
    <c:otherwise>
        <img src="${pageContext.request.contextPath}/img/false.gif" alt="False"/>
    </c:otherwise>
</c:choose>