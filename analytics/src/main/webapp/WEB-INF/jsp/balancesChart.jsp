<%@include file="includes/header.jsp" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<div class="container-fluid" ng-controller="BalancesChartController">
    <%--MENU--%>
    <tag:menu/>
    <%--END MENU--%>
    <c:if test="${pbUser != null}">
        <h2>Balances chart</h2>
        <strong>Select start month:</strong>
        <select ng-model="monthFrom" ng-change="monthRangeChanged()"
                ng-options="month as month.monthInYear + '/' + month.costYearAsNumber for month in costMonths">
            <option value="" disabled selected >Choose month from...</option>
        </select>

        <strong>Select end month:</strong>
        <select ng-model="monthTo" ng-change="monthRangeChanged()"
                ng-options="month as month.monthInYear + '/' + month.costYearAsNumber for month in costMonths">
            <option value="" disabled selected >Choose month to...</option>
        </select>

        <div class="panel panel-default" ng-if="chartData && chartData.length">
            <div class="panel-body">
                <canvas id="balancesChart" class="chart chart-bar" chart-dataset-override="datasetOverride"
                        chart-data="chartData" chart-labels="labels" chart-series="series">
                </canvas>
            </div>
        </div>
    </c:if>

    <c:if test="${pbUser == null}">
        <h1>Please login to portal</h1>
    </c:if>
</div>

<%@include file="includes/footer.jsp" %>