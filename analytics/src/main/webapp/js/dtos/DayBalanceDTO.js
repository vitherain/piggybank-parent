angular.module('piggy-bank-analytics').factory("DayBalanceDTO", ["AmountDTO", function(AmountDTO) {

    class DayBalanceDTO {

        constructor(arg) {
            this.amount = arg.amount ? new AmountDTO(arg.amount) : undefined;
            this.costYearId = arg.costYearId ? arg.costYearId : undefined;
            this.costYearAsNumber = arg.costYearAsNumber ? arg.costYearAsNumber : undefined;
            this.costMonthId = arg.costMonthId ? arg.costMonthId : undefined;
            this.costMonthMonthInYear = arg.costMonthMonthInYear ? arg.costMonthMonthInYear : undefined;
            this.dayInMonth = arg.dayInMonth ? arg.dayInMonth : undefined;
        }
    }

    return DayBalanceDTO;
}]);