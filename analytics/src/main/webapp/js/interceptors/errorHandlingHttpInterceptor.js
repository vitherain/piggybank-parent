angular.module('piggy-bank-analytics').factory("errorHandlingHttpInterceptor", ["$q", function($q) {

    var _response = function(response) {
        if (response.data && response.data.isErrorResponse) {
            return $q.reject(response);
        }

        return $q.resolve(response);
    };

    return {
        response: _response
    };
}]);