package cz.herain.piggybank.common.dto;

import java.io.Serializable;

/**
 * Created by Vít on 13. 11. 2016.
 */
public class FieldErrorDTO implements Serializable {

    private static final long serialVersionUID = -6245655314604522203L;

    private String fieldName;
    private Object rejectedValue;
    private String errorType;

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Object getRejectedValue() {
        return rejectedValue;
    }

    public void setRejectedValue(Object rejectedValue) {
        this.rejectedValue = rejectedValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FieldErrorDTO that = (FieldErrorDTO) o;

        if (fieldName != null ? !fieldName.equals(that.fieldName) : that.fieldName != null) return false;
        if (rejectedValue != null ? !rejectedValue.equals(that.rejectedValue) : that.rejectedValue != null)
            return false;
        return errorType != null ? errorType.equals(that.errorType) : that.errorType == null;

    }

    @Override
    public int hashCode() {
        int result = fieldName != null ? fieldName.hashCode() : 0;
        result = 31 * result + (rejectedValue != null ? rejectedValue.hashCode() : 0);
        result = 31 * result + (errorType != null ? errorType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FieldErrorDTO{" +
                "errorType='" + errorType + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", rejectedValue=" + rejectedValue +
                '}';
    }
}
