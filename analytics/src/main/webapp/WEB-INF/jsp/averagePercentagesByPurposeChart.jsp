<%@include file="includes/header.jsp" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<div class="container-fluid" ng-controller="AveragePercentagesByPurposeChartController">
    <%--MENU--%>
    <tag:menu/>
    <%--END MENU--%>
    <c:if test="${pbUser != null}">
        <h2>Average percentages by purpose chart</h2>
        <strong>Select start month:</strong>
        <select ng-model="monthFrom" ng-change="monthRangeChanged()"
                ng-options="month as month.monthInYear + '/' + month.costYearAsNumber for month in costMonths">
            <option value="" disabled selected >Choose month from...</option>
        </select>

        <strong>Select end month:</strong>
        <select ng-model="monthTo" ng-change="monthRangeChanged()"
                ng-options="month as month.monthInYear + '/' + month.costYearAsNumber for month in costMonths">
            <option value="" disabled selected >Choose month to...</option>
        </select>

        <h4 ng-if="incomesChartData && incomesChartData.length">Incomes</h4>
        <div class="panel panel-default" ng-if="incomesChartData && incomesChartData.length">
            <div class="panel-body">
                <canvas id="incomesPercentagesByPurposeChart" class="chart chart-pie"
                        chart-data="incomesChartData" chart-labels="incomesLabels" chart-options="options">
                </canvas>
            </div>
        </div>

        <h4 ng-if="expensesChartData && expensesChartData.length">Expenses</h4>
        <div class="panel panel-default" ng-if="expensesChartData && expensesChartData.length">
            <div class="panel-body">
                <canvas id="expensesPercentagesByPurposeChart" class="chart chart-pie"
                        chart-data="expensesChartData" chart-labels="expensesLabels" chart-options="options">
                </canvas>
            </div>
        </div>
    </c:if>

    <c:if test="${pbUser == null}">
        <h1>Please login to portal</h1>
    </c:if>
</div>
<%@include file="includes/footer.jsp" %>