angular.module('piggy-bank-analytics').factory("AmountDTO", [function() {

    class AmountDTO {

        constructor(arg) {
            this.amount = typeof arg.amount !== 'undefined' ? arg.amount : 0;
        }
    }

    return AmountDTO;
}]);