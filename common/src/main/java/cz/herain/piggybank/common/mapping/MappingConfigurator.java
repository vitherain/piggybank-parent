package cz.herain.piggybank.common.mapping;

import cz.herain.piggybank.common.dto.*;
import cz.herain.piggybank.common.entity.*;
import cz.herain.piggybank.common.mapping.utils.CustomMapperMap;
import ma.glasnost.orika.Converter;
import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;

import java.util.Map;

@Component
public class MappingConfigurator extends ConfigurableMapper implements ApplicationContextAware {

    public MappingConfigurator() {
        super(false);
    }

    private MapperFactory factory;

    private ApplicationContext applicationContext;

    private CustomMapperMap customMappers;

    @Override
    protected void configure(final MapperFactory factory) {
        this.factory = factory;
        configureConverters(applicationContext);
        configureCustomMappers();
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        this.init();
        configureClassMaps();
    }

    private void configureConverters(final ApplicationContext applicationContext) {
        final Map<String, Converter> converters = applicationContext.getBeansOfType(Converter.class);
        for (final Converter converter : converters.values()) {
            addConverter(converter);
        }
    }

    public void addConverter(final Converter<?, ?> converter) {
        factory.getConverterFactory().registerConverter(converter);
    }

    private void configureCustomMappers() {
        final Map<String, Mapper> mappers = applicationContext.getBeansOfType(Mapper.class);
        customMappers = new CustomMapperMap(mappers.values());
    }

    private void configureClassMaps() {
        factory.classMap(Expense.class, ExpenseDTO.class)
                .field("costMonth", "costMonthId")
                .field("purpose", "purposeId")
                .fieldAToB("purpose.name", "purposeName")
                .byDefault()
                .register();

        factory.classMap(Income.class, IncomeDTO.class)
                .field("costMonth", "costMonthId")
                .field("purpose", "purposeId")
                .fieldAToB("purpose.name", "purposeName")
                .byDefault()
                .register();

        factory.classMap(Purpose.class, PurposeDTO.class)
                .field("pbUser", "pbUserId")
                .byDefault()
                .register();

        factory.classMap(PbUser.class, PbUserDTO.class)
                .field("defaultPurpose", "defaultPurposeId")
                .byDefault()
                .register();

        factory.classMap(CostYear.class, CostYearDTO.class)
                .field("pbUser", "pbUserId")
                .byDefault()
                .register();

        factory.classMap(CostYear.class, CostYearWithMonthsDTO.class)
                .byDefault()
                .register();

        factory.classMap(CostMonth.class, CostMonthDTO.class)
                .field("costYear", "costYearId")
                .fieldAToB("costYear.year", "costYearAsNumber")
                .fieldAToB("costYear.pbUser.id", "pbUserId")
                .byDefault()
                .register();

        factory.classMap(CostMonth.class, CostMonthCompleteDTO.class)
                .field("costYear", "costYearId")
                .fieldAToB("costYear.year", "costYearAsNumber")
                .byDefault()
                .register();

        factory.classMap(CostMonth.class, BalanceDTO.class)
                .fieldAToB("costYear.id", "costYearId")
                .fieldAToB("costYear.year", "costYearAsNumber")
                .fieldAToB("id", "costMonthId")
                .fieldAToB("monthInYear", "costMonthMonthInYear")
                .register();

        factory.classMap(PurposeMiniDTO.class, SumsByPurposeDTO.class)
                .fieldAToB("name", "purposeName")
                .byDefault()
                .register();

        factory.classMap(ObjectError.class, FieldErrorDTO.class)
                .customize((Mapper<ObjectError, FieldErrorDTO>) customMappers.get(ObjectError.class, FieldErrorDTO.class))
                .register();
    }
}