angular.module('piggy-bank').service("modalService", ["$uibModal", function($uibModal) {

    this.isCancelReason = function(reason) {
        return reason === 'modalCancelled'
            || reason === 'backdrop click'
            || reason === 'escape key press';
    };

    this.openEditIncomeDialog = function(income, purposes) {

        var modalInstance = $uibModal.open({
            template:
                '<div class="modal-header">' +
                    '<h3 class="modal-title">Edit income</h3>' +
                '</div>' +

                '<div class="modal-body">' +
                    '<form name="incomeForm" ng-submit="ok()" novalidate>' +
                        '<div ng-messages="incomeForm.incomeDay.$error" ng-show="incomeForm.incomeDay.$dirty || incomeForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="min">Day must be at least 1!</span><br>' +
                            '<span ng-message="max">Day must be max 31!</span>' +
                            '<span ng-message="required">Day is required!</span>' +
                        '</div>' +
                        '<div ng-messages="incomeForm.incomePurpose.$error" ng-show="incomeForm.incomePurpose.$dirty || incomeForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="required">Purpose is required!</span>' +
                        '</div>' +
                        '<div ng-messages="incomeForm.incomeOriginator.$error" ng-show="incomeForm.incomeOriginator.$dirty || incomeForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="required">Originator is required!</span>' +
                        '</div>' +
                        '<div ng-messages="incomeForm.incomeAmount.$error" ng-show="incomeForm.incomeAmount.$dirty || incomeForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="min">Amount must be at least 1 CZK!</span>' +
                            '<span ng-message="required">Amount is required!</span>' +
                        '</div>' +
                        '<br>' +
                        '<!-- INPUTS -->' +
                        '<div class="col-xs-4">' +
                            'Day *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="incomeDay" type="number" ng-model="income.dayInMonth" required min="1" max="31">' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Purpose *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<select name="incomePurpose" required ng-model="income.purposeId" ng-options="prps.id as prps.name for prps in purposes">' +
                                '<option value="" disabled selected >Choose...</option>' +
                            '</select>' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Originator *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="incomeOriginator" type="text" ng-model="income.originator" maxlength="100" required>' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Amount *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="incomeAmount" type="number" ng-model="income.amount" required min="1">' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Note' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="incomeNote" type="text" ng-model="income.note" maxlength="100">' +
                        '</div>' +
                    '</form>' +
                '</div>' +

                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success" ng-click="ok()">Ok</button>' +
                    '<button type="button" class="btn btn-primary" ng-click="cancel()">Cancel</button>' +
                '</div>',
            controller: ["$scope", "$uibModalInstance", "income", "purposes",
                function($scope, $uibModalInstance, income, purposes) {
                    $scope.income = income;
                    $scope.purposes = purposes;

                    $scope.shouldBeOpen = true;

                    $scope.ok = function () {
                        if ($scope.incomeForm.$valid) {
                            $scope.shouldBeOpen = false;
                            $uibModalInstance.close($scope.income);
                        }
                    };

                    $scope.cancel = function () {
                        $scope.shouldBeOpen = false;
                        $uibModalInstance.dismiss('modalCancelled');
                    };
                }],
            size: 'md',
            resolve: {
                income: function () {
                    return income;
                },
                purposes: function () {
                    return purposes;
                }
            }
        });

        return modalInstance;
    };

    this.openEditExpenseDialog = function(expense, purposes) {

        var modalInstance = $uibModal.open({
            template:
                '<div class="modal-header">' +
                    '<h3 class="modal-title">Edit expense</h3>' +
                '</div>' +

                '<div class="modal-body">' +
                    '<form name="expenseForm" ng-submit="ok()" novalidate>' +
                        '<div ng-messages="expenseForm.expenseDay.$error" ng-show="expenseForm.expenseDay.$dirty || expenseForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="min">Day must be at least 1!</span><br>' +
                            '<span ng-message="max">Day must be max 31!</span>' +
                            '<span ng-message="required">Day is required!</span>' +
                        '</div>' +
                        '<div ng-messages="expenseForm.expensePurpose.$error" ng-show="expenseForm.expensePurpose.$dirty || expenseForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="required">Purpose is required!</span>' +
                        '</div>' +
                        '<div ng-messages="expenseForm.expenseRecipient.$error" ng-show="expenseForm.expenseRecipient.$dirty || expenseForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="required">Recipient is required!</span>' +
                        '</div>' +
                        '<div ng-messages="expenseForm.expenseAmount.$error" ng-show="expenseForm.expenseAmount.$dirty || expenseForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="min">Amount must be at least 1 CZK!</span>' +
                            '<span ng-message="required">Amount is required!</span>' +
                        '</div>' +
                        '<br>' +
                        '<!-- INPUTS -->' +
                        '<div class="col-xs-4">' +
                            'Day *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="expenseDay" type="number" ng-model="expense.dayInMonth" required min="1" max="31">' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Purpose *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<select name="expensePurpose" required ng-model="expense.purposeId" ng-options="prps.id as prps.name for prps in purposes">' +
                                '<option value="" disabled selected >Choose...</option>' +
                            '</select>' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Recipient *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="expenseRecipient" type="text" ng-model="expense.recipient" maxlength="100" required>' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Amount *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="expenseAmount" type="number" ng-model="expense.amount" required min="1">' +
                        '</div>' +
                        '<div class="col-xs-4">' +
                            'Note' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="expenseNote" type="text" ng-model="expense.note" maxlength="100">' +
                        '</div>' +
                    '</form>' +
                '</div>' +

                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success" ng-click="ok()">Ok</button>' +
                    '<button type="button" class="btn btn-primary" ng-click="cancel()">Cancel</button>' +
                '</div>',
            controller: ["$scope", "$uibModalInstance", "expense", "purposes",
                function($scope, $uibModalInstance, expense, purposes) {
                    $scope.expense = expense;
                    $scope.purposes = purposes;

                    $scope.shouldBeOpen = true;

                    $scope.ok = function () {
                        if ($scope.expenseForm.$valid) {
                            $scope.shouldBeOpen = false;
                            $uibModalInstance.close($scope.expense);
                        }
                    };

                    $scope.cancel = function () {
                        $scope.shouldBeOpen = false;
                        $uibModalInstance.dismiss('modalCancelled');
                    };
                }],
            size: 'md',
            resolve: {
                expense: function () {
                    return expense;
                },
                purposes: function () {
                    return purposes;
                }
            }
        });

        return modalInstance;
    };

    this.openAddYearDialog = function(year, knownYears) {

        var modalInstance = $uibModal.open({
            template:
                '<div class="modal-header">' +
                    '<h3 class="modal-title">Edit cost year</h3>' +
                '</div>' +

                '<div class="modal-body">' +
                    '<form name="yearForm" ng-submit="ok()" novalidate>' +
                        '<div ng-messages="yearForm.year.$error" ng-show="yearForm.year.$dirty || yearForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="required">Year is required!</span>' +
                            '<span ng-message="min">Year must be higher than 1970!</span>' +
                            '<span ng-message="max">Year must be max 2150!</span>' +
                            '<span ng-message="isUnique">Year must be unique!</span>' +
                        '</div>' +
                        '<br>' +
                        '<!-- INPUTS -->' +
                        '<div class="col-xs-4">' +
                            'Year *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="year" type="number" ng-model="year.year" is-unique="knownYears" required min="1970" max="2150">' +
                        '</div>' +
                    '</form>' +
                '</div>' +

                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success" ng-click="ok()">Ok</button>' +
                    '<button type="button" class="btn btn-primary" ng-click="cancel()">Cancel</button>' +
                '</div>',
            controller: ["$scope", "$uibModalInstance", "year", "knownYears",
                function($scope, $uibModalInstance, year, knownYears) {
                    $scope.year = year;
                    $scope.knownYears = knownYears;

                    $scope.shouldBeOpen = true;

                    $scope.ok = function () {
                        if ($scope.yearForm.$valid) {
                            $scope.shouldBeOpen = false;
                            $uibModalInstance.close($scope.year);
                        }
                    };

                    $scope.cancel = function () {
                        $scope.shouldBeOpen = false;
                        $uibModalInstance.dismiss('modalCancelled');
                    };
                }],
            size: 'md',
            resolve: {
                year: function () {
                    return year;
                },
                knownYears: function() {
                    return knownYears;
                }
            }
        });

        return modalInstance;
    };

    this.openAddMonthDialog = function(month, knownMonths) {

        var modalInstance = $uibModal.open({
            template:
                '<div class="modal-header">' +
                    '<h3 class="modal-title">Edit cost month</h3>' +
                '</div>' +

                '<div class="modal-body">' +
                    '<form name="monthForm" ng-submit="ok()" novalidate>' +
                        '<div ng-messages="monthForm.monthInYear.$error" ng-show="monthForm.monthInYear.$dirty || monthForm.$submitted" class="col-xs-12 validation-messages">' +
                            '<span ng-message="required">Month is required!</span>' +
                            '<span ng-message="min">Month must be higher than 1!</span>' +
                            '<span ng-message="max">Month must be max 12!</span>' +
                            '<span ng-message="isUnique">Month must be unique!</span>' +
                        '</div>' +
                        '<br>' +
                        '<!-- INPUTS -->' +
                        '<div class="col-xs-4">' +
                            'Month *' +
                        '</div>' +
                        '<div class="col-xs-8">' +
                            '<input name="monthInYear" type="number" ng-model="month.monthInYear" is-unique="knownMonths" required min="1" max="12">' +
                        '</div>' +
                    '</form>' +
                '</div>' +

                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-success" ng-click="ok()">Ok</button>' +
                    '<button type="button" class="btn btn-primary" ng-click="cancel()">Cancel</button>' +
                '</div>',
            controller: ["$scope", "$uibModalInstance", "month", "knownMonths",
                function($scope, $uibModalInstance, month, knownMonths) {
                    $scope.month = month;
                    $scope.knownMonths = knownMonths;

                    $scope.shouldBeOpen = true;

                    $scope.ok = function () {
                        if ($scope.monthForm.$valid) {
                            $scope.shouldBeOpen = false;
                            $uibModalInstance.close($scope.month);
                        }
                    };

                    $scope.cancel = function () {
                        $scope.shouldBeOpen = false;
                        $uibModalInstance.dismiss('modalCancelled');
                    };
                }],
            size: 'md',
            resolve: {
                month: function () {
                    return month;
                },
                knownMonths: function() {
                    return knownMonths;
                }
            }
        });

        return modalInstance;
    };
}]);