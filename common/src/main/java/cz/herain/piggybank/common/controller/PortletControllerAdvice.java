package cz.herain.piggybank.common.controller;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import cz.herain.piggybank.common.dto.ErrorDTO;
import cz.herain.piggybank.common.dto.FieldErrorDTO;
import cz.herain.piggybank.common.dto.ValidationErrorDTO;
import cz.herain.piggybank.common.entity.PbUser;
import cz.herain.piggybank.common.exception.UnexpectedException;
import cz.herain.piggybank.common.exception.ValidationException;
import cz.herain.piggybank.common.repository.PbUserRepository;
import cz.herain.piggybank.common.util.CommonUtils;
import cz.herain.piggybank.common.util.ContextUtil;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.portlet.MimeResponse;
import javax.portlet.PortletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static cz.herain.piggybank.common.util.CommonConstants.PB_USER_ATTRIBUTE_NAME;

public abstract class PortletControllerAdvice {

    @Autowired
    protected ContextUtil contextUtil;

    @Autowired
    protected PbUserRepository pbUserRepository;

    @Autowired
    private MapperFacade mapper;

    @ModelAttribute(PB_USER_ATTRIBUTE_NAME)
    public PbUser pbUserModelAttribute(PortletRequest portletRequest)
            throws SystemException, PortalException {
        User currentUser = PortalUtil.getUser(portletRequest);

        if (currentUser != null) {
            return getPbUserForLiferayUser(currentUser);
        }

        return null;
    }

    private PbUser getPbUserForLiferayUser(User user) {
        PbUser pbUser = pbUserRepository.findByUserId(user.getUserId());

        if (pbUser == null) {
            pbUser = createAndSavePbUserForLiferayUser(user);
        }

        return pbUser;
    }

    private PbUser createAndSavePbUserForLiferayUser(User user) {
        PbUser pbUser = new PbUser();
        pbUser.setUserId(user.getUserId());

        return pbUserRepository.save(pbUser);
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @ExceptionHandler(UnexpectedException.class)
    public void unexpectedExceptionHandler(PortletRequest request,
                                           MimeResponse response,
                                           UnexpectedException ex) throws IOException {
        ErrorDTO errorDTO = ErrorDTO.ErrorDTOBuilder.anErrorDTO()
                .withMessage(String.format("%s: %s",
                        ex.getClass().getSimpleName(),
                        StringUtils.isNotEmpty(ex.getMessage()) ? ex.getMessage() : StringUtils.EMPTY))
                .withStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
                .withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();

        CommonUtils.writeAsJson(response, errorDTO);
    }

    @ExceptionHandler(ValidationException.class)
    public void validationExceptionHandler(PortletRequest request,
                                           MimeResponse response,
                                           ValidationException ex) throws IOException {

        List<FieldErrorDTO> fieldErrors;
        if (ex.getErrors() != null && ex.getErrors().hasErrors()) {
            fieldErrors = mapper.mapAsList(ex.getErrors().getAllErrors(), FieldErrorDTO.class);
        } else {
            fieldErrors = new ArrayList<>();
        }

        ValidationErrorDTO errorDTO = ValidationErrorDTO.ValidationErrorDTOBuilder.aValidationErrorDTO()
                .withMessage(String.format("%s: %s",
                        ex.getClass().getSimpleName(),
                        StringUtils.isNotEmpty(ex.getMessage()) ? ex.getMessage() : StringUtils.EMPTY))
                .withStatusText(HttpStatus.BAD_REQUEST)
                .withStatus(HttpStatus.BAD_REQUEST.value())
                .withFieldErrors(fieldErrors)
                .build();

        CommonUtils.writeAsJson(response, errorDTO);
    }

    @ExceptionHandler(Exception.class)
    public void exceptionHandler(PortletRequest request,
                                 MimeResponse response,
                                 Exception ex) throws IOException {
        ErrorDTO errorDTO = ErrorDTO.ErrorDTOBuilder.anErrorDTO()
                .withMessage(String.format("%s: %s",
                        ex.getClass().getSimpleName(),
                        StringUtils.isNotEmpty(ex.getMessage()) ? ex.getMessage() : StringUtils.EMPTY))
                .withStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
                .withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();

        CommonUtils.writeAsJson(response, errorDTO);
    }
}
