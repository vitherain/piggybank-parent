package cz.herain.piggybank.common.repository;

import cz.herain.piggybank.common.entity.Purpose;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PurposeRepository extends BaseRepository<Purpose> {

    @Query("SELECT p FROM Purpose p WHERE p.pbUser.id = :userId ORDER BY p.name ASC")
    List<Purpose> findByUserIdSortedByNameAsc(@Param("userId") long userId);

    @Query("SELECT p FROM Purpose p WHERE p.pbUser.id = :userId AND p.active = 'TRUE' ORDER BY p.name ASC")
    List<Purpose> findActiveByUserIdSortedByNameAsc(@Param("userId") long userId);

    @Query("SELECT new Purpose(p.id, p.name) FROM Purpose p WHERE p.pbUser.id = :userId AND p.active = 'TRUE' ORDER BY p.name ASC")
    List<Purpose> findMinimalActiveByUserIdSortedByNameAsc(@Param("userId") long userId);

    @Query("SELECT new Purpose(p.id, p.name) FROM Purpose p WHERE p.pbUser.id = :userId ORDER BY p.name ASC")
    List<Purpose> findMinimalByUserIdSortedByNameAsc(@Param("userId") long userId);

    @Query(
            "SELECT new Purpose(p.id, p.name) FROM Purpose p WHERE EXISTS "
            + "(SELECT e.purpose FROM Expense e WHERE e.costMonth.id = :monthId AND e.purpose = p)")
    List<Purpose> findPurposesUsedForExpensesInMonth(@Param("monthId") long monthId);

    @Query(
            "SELECT new Purpose(p.id, p.name) FROM Purpose p WHERE EXISTS "
            + "(SELECT i.purpose FROM Income i WHERE i.costMonth.id = :monthId AND i.purpose = p)")
    List<Purpose> findPurposesUsedForIncomesInMonth(@Param("monthId") long monthId);

    @Query(
            "SELECT new Purpose(p.id, p.name) FROM Purpose p WHERE EXISTS "
            + "(SELECT e.purpose FROM Expense e WHERE e.costMonth.id in :costMonthIds AND e.purpose = p)")
    List<Purpose> findPurposesUsedForExpensesInMonths(@Param("costMonthIds") List<Long> costMonthIds);

    @Query(
            "SELECT new Purpose(p.id, p.name) FROM Purpose p WHERE EXISTS "
            + "(SELECT i.purpose FROM Income i WHERE i.costMonth.id in :costMonthIds AND i.purpose = p)")
    List<Purpose> findPurposesUsedForIncomesInMonths(@Param("costMonthIds") List<Long> costMonthIds);
}
