package cz.herain.piggybank.common.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vit Herain on 06/11/2016.
 */
public class PercentagesByPurposeSummaryDTO implements Serializable {

    private List<PercentagesByPurposeDTO> expenses;
    private List<PercentagesByPurposeDTO> incomes;

    public PercentagesByPurposeSummaryDTO() {
        this.expenses = new ArrayList<>();
        this.incomes = new ArrayList<>();
    }

    public List<PercentagesByPurposeDTO> getExpenses() {
        return expenses;
    }

    public void setExpenses(final List<PercentagesByPurposeDTO> expenses) {
        this.expenses = expenses;
    }

    public List<PercentagesByPurposeDTO> getIncomes() {
        return incomes;
    }

    public void setIncomes(final List<PercentagesByPurposeDTO> incomes) {
        this.incomes = incomes;
    }

    @Override
    public String toString() {
        return "PercentagesByPurposeSummaryDTO{" +
                "expenses=" + expenses +
                ", incomes=" + incomes +
                '}';
    }
}
