package cz.herain.piggybank.common.repository;

import cz.herain.piggybank.common.entity.Expense;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.List;

public interface ExpenseRepository extends BaseRepository<Expense> {

    @Query("SELECT e FROM Expense e WHERE e.costMonth.id = :monthId ORDER BY e.dayInMonth ASC")
    List<Expense> findByMonthIdSortedByDayDesc(@Param("monthId") long monthId);

    @Query(
            value = "SELECT sum(amount) FROM expense WHERE cost_month_id = :monthId",
            nativeQuery = true
    )
    BigInteger countSumAmountByCostMonthId(@Param("monthId") long monthId);

    @Query(
            value =
                    "SELECT COALESCE(SUM(e.amount), 0) / :costMonthsCount " +
                    "FROM (" +
                            "SELECT * FROM expense WHERE (cost_month_id in (:costMonthIds))" +
                    ") e " +
                    "RIGHT OUTER JOIN purpose p ON e.purpose_id = p.id " +
                    "WHERE p.user_id = :userId " +
                    "GROUP BY p.name " +
                    "ORDER BY p.name",

            nativeQuery = true
    )
    List<BigInteger> averagesByPurposeAndCostMonthsSortedByPurposeName(
            @Param("costMonthIds") List<Long> costMonthIds,
            @Param("costMonthsCount") int costMonthsCount,
            @Param("userId") long userId);

    @Query(
            value =
                    "SELECT COALESCE(SUM(e.amount), 0) " +
                    "FROM expense e " +
                    "WHERE e.purpose_id = :purposeId AND e.cost_month_id = :monthId",
            nativeQuery = true
    )
    BigInteger countSumByPurposeIdAndMonthId(@Param("purposeId") Long purposeId, @Param("monthId") long monthId);

    @Query(
            value =
                    "SELECT COALESCE(SUM(e.amount), 0) " +
                            "FROM expense e " +
                            "WHERE e.purpose_id = :purposeId AND e.cost_month_id in :costMonthIds",
            nativeQuery = true
    )
    BigInteger countSumByPurposeIdAndMonthIds(@Param("purposeId") long purposeId, @Param("costMonthIds") List<Long> costMonthIds);

    @Query(
            value = "SELECT sum(amount) FROM expense WHERE cost_month_id in :costMonthIds",
            nativeQuery = true
    )
    BigInteger countSumAmountByCostMonthIds(@Param("costMonthIds") List<Long> costMonthIds);
}
