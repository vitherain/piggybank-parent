package cz.herain.piggybank.analytics.controller;

import cz.herain.piggybank.analytics.service.AnalyticService;
import cz.herain.piggybank.common.controller.PortletControllerAdvice;
import cz.herain.piggybank.common.dto.BalanceDTO;
import cz.herain.piggybank.common.dto.PercentagesByPurposeSummaryDTO;
import cz.herain.piggybank.common.dto.SumsByPurposeDTO;
import cz.herain.piggybank.common.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class AnalyticDataController extends PortletControllerAdvice {

    @Autowired
    private AnalyticService analyticService;

    @ResourceMapping("usersBalancesBetweenCostMonths")
    public void getUsersBalancesBetweenCostMonths(@RequestParam(value = "monthFromId") long monthFromId,
                                                  @RequestParam(value = "monthToId") long monthToId,
                                                  ResourceRequest request,
                                                  ResourceResponse response) throws IOException {

        List<BalanceDTO> balances = analyticService.getBalancesInIntervalOfCostMonths(monthFromId, monthToId);

        CommonUtils.writeAsJson(response, balances);
    }

    @ResourceMapping("usersSumsByBurposeInCostMonth")
    public void getUsersSumsByBurposeInCostMonth(@RequestParam(value = "monthId") long monthId,
                                                  ResourceRequest request,
                                                  ResourceResponse response) throws IOException {

        List<SumsByPurposeDTO> purposeSums = analyticService.getPurposeSumsByCostMonthSortedByPurposeName(monthId);

        CommonUtils.writeAsJson(response, purposeSums);
    }

    @ResourceMapping("usersAveragesByBurposeInCostMonth")
    public void getUsersAveragesByBurposeInCostMonth(@RequestParam(value = "monthFromId") long monthFromId,
                                                     @RequestParam(value = "monthToId") long monthToId,
                                                     ResourceRequest request,
                                                     ResourceResponse response) throws IOException {

        List<SumsByPurposeDTO> purposeAvgs = analyticService
                .getPurposeAveragesInIntervalOfCostMonthsSortedByPurposeName(monthFromId, monthToId);

        CommonUtils.writeAsJson(response, purposeAvgs);
    }

    @ResourceMapping("usersCashFlowInCostMonth")
    public void getUsersCashFlowInCostMonth(@RequestParam(value = "monthId") long monthId,
                                            ResourceRequest request,
                                            ResourceResponse response) throws IOException {

        List<BalanceDTO> cashFlowInfo = analyticService.getDayBalancesInMonth(monthId);

        CommonUtils.writeAsJson(response, cashFlowInfo);
    }

    @ResourceMapping("usersPercentagesByPurposeInMonth")
    public void getUsersPercentagesByPurposeInMonth(@RequestParam(value = "monthId") long monthId,
                                            ResourceRequest request,
                                            ResourceResponse response) throws IOException {

        PercentagesByPurposeSummaryDTO percentages = analyticService.getPercentagesByPurposeInMonth(monthId);

        CommonUtils.writeAsJson(response, percentages);
    }

    @ResourceMapping("usersAveragePercentagesByPurposeInMonth")
    public void getUsersAveragePercentagesByPurposeInMonth(@RequestParam(value = "monthFromId") long monthFromId,
                                                           @RequestParam(value = "monthToId") long monthToId,
                                                           ResourceRequest request,
                                                           ResourceResponse response) throws IOException {

        PercentagesByPurposeSummaryDTO percentages = analyticService
                .getAveragePercentagesByPurposeInIntervalOfCostMonths(monthFromId, monthToId);

        CommonUtils.writeAsJson(response, percentages);
    }
}
