package cz.herain.piggybank.common.service.base;

public interface BaseDeleteService {

    void delete(long id);
}
