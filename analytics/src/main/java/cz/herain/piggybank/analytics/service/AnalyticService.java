package cz.herain.piggybank.analytics.service;

import cz.herain.piggybank.common.dto.AmountDTO;
import cz.herain.piggybank.common.dto.BalanceDTO;
import cz.herain.piggybank.common.dto.PercentagesByPurposeSummaryDTO;
import cz.herain.piggybank.common.dto.SumsByPurposeDTO;

import java.util.List;

/**
 * Created by Vit Herain on 03/11/2016.
 */
public interface AnalyticService {

    List<BalanceDTO> getBalancesInIntervalOfCostMonths(long monthFromId, long monthToId);

    List<SumsByPurposeDTO> getPurposeSumsByCostMonthSortedByPurposeName(long monthId);

    List<SumsByPurposeDTO> getPurposeAveragesInIntervalOfCostMonthsSortedByPurposeName(long monthFromId, long monthToId);

    List<BalanceDTO> getDayBalancesInMonth(long monthId);

    AmountDTO getBalanceInOneDayByMonthId(int dayInMonth, long monthId);

    PercentagesByPurposeSummaryDTO getPercentagesByPurposeInMonth(long monthId);

    PercentagesByPurposeSummaryDTO getAveragePercentagesByPurposeInIntervalOfCostMonths(long monthFromId, long monthToId);
}
