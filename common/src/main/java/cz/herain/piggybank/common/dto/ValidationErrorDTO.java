package cz.herain.piggybank.common.dto;

import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vít on 13. 11. 2016.
 */
public class ValidationErrorDTO implements Serializable {

    private static final long serialVersionUID = -6245655314604522203L;

    private final boolean isErrorResponse = true;
    private HttpStatus statusText;
    private int status;
    private String message;
    private List<FieldErrorDTO> fieldErrors;

    public List<FieldErrorDTO> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<FieldErrorDTO> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatusText() {
        return statusText;
    }

    public void setStatusText(HttpStatus statusText) {
        this.statusText = statusText;
    }

    public boolean isErrorResponse() {
        return isErrorResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValidationErrorDTO that = (ValidationErrorDTO) o;

        if (isErrorResponse != that.isErrorResponse) return false;
        if (status != that.status) return false;
        if (statusText != that.statusText) return false;
        return message != null ? message.equals(that.message) : that.message == null;

    }

    @Override
    public int hashCode() {
        int result = (isErrorResponse ? 1 : 0);
        result = 31 * result + (statusText != null ? statusText.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ValidationErrorDTO{" +
                "fieldErrors=" + fieldErrors +
                ", isErrorResponse=" + isErrorResponse +
                ", statusText=" + statusText +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }

    public static class ValidationErrorDTOBuilder {
        private List<FieldErrorDTO> fieldErrors;
        private HttpStatus statusText;
        private int status;
        private String message;

        private ValidationErrorDTOBuilder() {
        }

        public static ValidationErrorDTOBuilder aValidationErrorDTO() {
            return new ValidationErrorDTOBuilder();
        }

        public ValidationErrorDTOBuilder withFieldErrors(List<FieldErrorDTO> fieldErrors) {
            this.fieldErrors = fieldErrors;
            return this;
        }

        public ValidationErrorDTOBuilder withStatusText(HttpStatus statusText) {
            this.statusText = statusText;
            return this;
        }

        public ValidationErrorDTOBuilder withStatus(int status) {
            this.status = status;
            return this;
        }

        public ValidationErrorDTOBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public ValidationErrorDTOBuilder but() {
            return aValidationErrorDTO().withFieldErrors(fieldErrors).withStatusText(statusText).withStatus(status).withMessage(message);
        }

        public ValidationErrorDTO build() {
            ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
            validationErrorDTO.setFieldErrors(fieldErrors);
            validationErrorDTO.setStatusText(statusText);
            validationErrorDTO.setStatus(status);
            validationErrorDTO.setMessage(message);
            return validationErrorDTO;
        }
    }
}
