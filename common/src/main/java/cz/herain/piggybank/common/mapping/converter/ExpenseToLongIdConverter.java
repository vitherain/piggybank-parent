package cz.herain.piggybank.common.mapping.converter;

import cz.herain.piggybank.common.entity.Expense;
import org.springframework.stereotype.Component;

@Component
public class ExpenseToLongIdConverter extends AbstractEntityToIdConverter<Expense, Long> {
}
