package cz.herain.piggybank.common.service.base;

public interface BaseGetOneService<DTO> {

    DTO getOne(long id);
}
