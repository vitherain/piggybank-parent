package cz.herain.piggybank.common.dto;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * Created by Vít on 13. 11. 2016.
 */
public class ErrorDTO implements Serializable {

    private static final long serialVersionUID = -6245655314604522203L;

    private final boolean isErrorResponse = true;
    private HttpStatus statusText;
    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatusText() {
        return statusText;
    }

    public void setStatusText(HttpStatus statusText) {
        this.statusText = statusText;
    }

    public boolean isErrorResponse() {
        return isErrorResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ErrorDTO errorDTO = (ErrorDTO) o;

        if (isErrorResponse != errorDTO.isErrorResponse) return false;
        if (status != errorDTO.status) return false;
        if (statusText != errorDTO.statusText) return false;
        return message != null ? message.equals(errorDTO.message) : errorDTO.message == null;

    }

    @Override
    public int hashCode() {
        int result = (isErrorResponse ? 1 : 0);
        result = 31 * result + (statusText != null ? statusText.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ErrorDTO{" +
                "isErrorResponse=" + isErrorResponse +
                ", statusText=" + statusText +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }

    public static class ErrorDTOBuilder {
        private String message;
        private HttpStatus statusText;
        private int status;

        private ErrorDTOBuilder() {
        }

        public static ErrorDTOBuilder anErrorDTO() {
            return new ErrorDTOBuilder();
        }

        public ErrorDTOBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public ErrorDTOBuilder withStatusText(HttpStatus statusText) {
            this.statusText = statusText;
            return this;
        }

        public ErrorDTOBuilder withStatus(int status) {
            this.status = status;
            return this;
        }

        public ErrorDTOBuilder but() {
            return anErrorDTO().withMessage(message).withStatusText(statusText).withStatus(status);
        }

        public ErrorDTO build() {
            ErrorDTO errorDTO = new ErrorDTO();
            errorDTO.setMessage(message);
            errorDTO.setStatusText(statusText);
            errorDTO.setStatus(status);
            return errorDTO;
        }
    }
}
