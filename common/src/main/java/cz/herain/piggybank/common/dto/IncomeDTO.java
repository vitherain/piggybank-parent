package cz.herain.piggybank.common.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static cz.herain.piggybank.common.util.ValidationConstants.*;

public class IncomeDTO extends BaseDTO {

    @Min(value = MIN_DAY_IN_MONTH)
    @Max(value = MAX_DAY_IN_MONTH)
    private int dayInMonth;

    @NotNull
    private Long costMonthId;
    @NotNull
    private Long purposeId;
    private String purposeName;
    @Min(value = MIN_AMOUNT)
    private int amount;

    @Size(max = STRING_MAX_LENGTH)
    private String note;

    @Size(max = STRING_MAX_LENGTH)
    private String originator;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Long getCostMonthId() {
        return costMonthId;
    }

    public void setCostMonthId(Long costMonthId) {
        this.costMonthId = costMonthId;
    }

    public int getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(int dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public Long getPurposeId() {
        return purposeId;
    }

    public void setPurposeId(Long purposeId) {
        this.purposeId = purposeId;
    }

    public String getPurposeName() {
        return purposeName;
    }

    public void setPurposeName(String purposeName) {
        this.purposeName = purposeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        IncomeDTO incomeDTO = (IncomeDTO) o;

        if (dayInMonth != incomeDTO.dayInMonth) return false;
        if (amount != incomeDTO.amount) return false;
        if (costMonthId != null ? !costMonthId.equals(incomeDTO.costMonthId) : incomeDTO.costMonthId != null)
            return false;
        if (purposeId != null ? !purposeId.equals(incomeDTO.purposeId) : incomeDTO.purposeId != null) return false;
        if (note != null ? !note.equals(incomeDTO.note) : incomeDTO.note != null) return false;
        if (purposeName != null ? !purposeName.equals(incomeDTO.purposeName) : incomeDTO.purposeName != null) return false;
        return originator != null ? originator.equals(incomeDTO.originator) : incomeDTO.originator == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + dayInMonth;
        result = 31 * result + (costMonthId != null ? costMonthId.hashCode() : 0);
        result = 31 * result + (purposeId != null ? purposeId.hashCode() : 0);
        result = 31 * result + amount;
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (purposeName != null ? purposeName.hashCode() : 0);
        result = 31 * result + (originator != null ? originator.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IncomeDTO{" +
                "amount=" + amount +
                ", dayInMonth=" + dayInMonth +
                ", costMonthId=" + costMonthId +
                ", purposeId=" + purposeId +
                ", note='" + note + '\'' +
                ", purposeName='" + purposeName + '\'' +
                ", originator='" + originator + '\'' +
                '}';
    }
}
