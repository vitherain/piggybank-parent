angular.module('piggy-bank-analytics').controller("SumsByPurposeChartController", ["$scope", "$q", "httpFacade", "toasty", "arrayUtils", "SumsByPurposeDTO",
    function($scope, $q, httpFacade, toasty, arrayUtils, SumsByPurposeDTO) {

    function reloadUsersCostPeriods() {
        httpFacade.getResource("usersCostMonths").then(function(response) {
            $scope.costMonths = response.data;
        });
    }
    reloadUsersCostPeriods();

    $scope.monthChanged = function() {
        httpFacade.getResource("usersSumsByBurposeInCostMonth", {monthId: $scope.costMonth})
            .then(function(response) {
                return response.data.map(function(object) {
                    return new SumsByPurposeDTO(object);
                });
            }).then(function(purposeSums) {
                prepareChartData(purposeSums);
            });
    };

    function prepareChartData(data) {
        $scope.labels = arrayUtils.getSubArrayBySelectorIgnoreIfNull(data, function(item) {
            return item.show ? item.purposeName : null;
        });
        $scope.purposes = data;

        $scope.chartData = [
            arrayUtils.getSubArrayBySelectorIgnoreIfNull(data, function(item) {
                return item.show ? item.incomesAmount.amount : null;
            }),
            arrayUtils.getSubArrayBySelectorIgnoreIfNull(data, function(item) {
                return item.show ? item.expensesAmount.amount : null;
            })
        ];
    }

    $scope.refreshData = function() {
        prepareChartData($scope.purposes);
    };

    $scope.series = ['Incomes [CZK]', 'Expenses [CZK]'];

    $scope.datasetOverride = [
        {
            backgroundColor: "rgba(177,229,112,0.7)"
        },
        {
            backgroundColor: "rgba(255,99,132,0.7)"
        }
    ];
}]);