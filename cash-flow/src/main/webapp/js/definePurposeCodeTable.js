$(function() {
    $("#" + cashFlowPortletNamespace + "activeCheckbox").change(function() {
        var $activeCheckBox = $(this),
            $defaultCheckbox = $("#" + cashFlowPortletNamespace + "defaultForCurrentUserCheckbox");

        if (isUnchecked($activeCheckBox)) {
            uncheck($defaultCheckbox);
            disable($defaultCheckbox);
        } else {
            enable($defaultCheckbox);
        }
    });

    function isUnchecked($checkbox) {
        return !$checkbox.is(":checked");
    }

    function uncheck($checkbox) {
        return $checkbox.prop("checked", false);
    }

    function disable($checkbox) {
        $checkbox.prop("disabled", true);
    }

    function enable($checkbox) {
        $checkbox.prop("disabled", false);
    }
});