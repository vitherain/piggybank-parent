package cz.herain.piggybank.common.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cost_year")
public class CostYear extends BaseEntity {

    @Column(name = "year", nullable = false)
    private int year;

    @OneToMany(mappedBy = "costYear", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<CostMonth> costMonths;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private PbUser pbUser;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<CostMonth> getCostMonths() {
        return costMonths;
    }

    public void setCostMonths(List<CostMonth> costMonths) {
        this.costMonths = costMonths;
    }

    public PbUser getPbUser() {
        return pbUser;
    }

    public void setPbUser(PbUser pbUser) {
        this.pbUser = pbUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CostYear costYear = (CostYear) o;

        if (year != costYear.year) return false;
        return pbUser != null ? pbUser.equals(costYear.pbUser) : costYear.pbUser == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + year;
        result = 31 * result + (pbUser != null ? pbUser.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CostYear{" +
                "id=" + id +
                ", year=" + year +
                ", pbUserId=" + (pbUser != null ? pbUser.getId() : null) +
                '}';
    }
}
