<%@include file="includes/header.jsp" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<div class="container-fluid" ng-controller="MonthCashFlowChartController">
    <%--MENU--%>
    <tag:menu/>
    <%--END MENU--%>
    <c:if test="${pbUser != null}">
        <h2>Month cash flow chart</h2>
        <strong>Select month:</strong>
        <select ng-model="costMonth" ng-change="monthChanged()"
                ng-options="month.id as month.monthInYear + '/' + month.costYearAsNumber for month in costMonths">
            <option value="" disabled selected >Choose month...</option>
        </select>

        <div class="panel panel-default" ng-if="chartData && chartData.length">
            <div class="panel-body">
                <canvas id="monthCashFlowChart" class="chart chart-line" chart-dataset-override="datasetOverride"
                        chart-data="chartData" chart-labels="labels" chart-series="series">
                </canvas>
            </div>
        </div>
    </c:if>

    <c:if test="${pbUser == null}">
        <h1>Please login to portal</h1>
    </c:if>
</div>

<%@include file="includes/footer.jsp" %>