package cz.herain.piggybank.common.service.impl;

import cz.herain.piggybank.common.service.CostYearService;
import cz.herain.piggybank.common.service.base.impl.AbstractCommonService;
import cz.herain.piggybank.common.dto.CostYearDTO;
import cz.herain.piggybank.common.dto.CostYearWithMonthsDTO;
import cz.herain.piggybank.common.entity.CostYear;
import cz.herain.piggybank.common.repository.CostYearRepository;
import cz.herain.piggybank.common.util.ContextUtil;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
@Service
public class CostYearServiceImpl
        extends AbstractCommonService<CostYearDTO, CostYearDTO, CostYearDTO, CostYearDTO, CostYear>
        implements CostYearService {

    @Autowired
    private CostYearRepository costYearRepository;

    @Autowired
    private ContextUtil contextUtil;

    @Autowired
    private MapperFacade mapper;

    public CostYearServiceImpl() {
        super(CostYearDTO.class, CostYearDTO.class, CostYearDTO.class, CostYearDTO.class, CostYear.class);
    }

    @Override
    public CostYearDTO saveOrUpdate(final CostYearDTO dto) {
        final long currentUserId = contextUtil.getCurrentPbUserId();
        dto.setPbUserId(currentUserId);

        CostYear entity = mapper.map(dto, CostYear.class);
        entity = costYearRepository.save(entity);

        return mapper.map(entity, CostYearDTO.class);
    }

    @Override
    public List<CostYearDTO> getAllByUserIdSortedByYearDesc(final long userId) {
        List<CostYear> costYears = costYearRepository.findByUserIdSortedByYearDesc(userId);
        return mapper.mapAsList(costYears, CostYearDTO.class);
    }

    @Override
    public List<CostYearWithMonthsDTO> getAllWithMonthsByUserIdSortedByYearDesc(final long userId) {
        List<CostYear> costYears = costYearRepository.findByUserIdSortedByYearDesc(userId);
        return mapper.mapAsList(costYears, CostYearWithMonthsDTO.class);
    }
}
