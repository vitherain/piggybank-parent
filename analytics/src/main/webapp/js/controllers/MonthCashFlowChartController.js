angular.module('piggy-bank-analytics').controller("MonthCashFlowChartController", ["$scope", "$q", "httpFacade", "toasty", "arrayUtils", "DayBalanceDTO",
    function($scope, $q, httpFacade, toasty, arrayUtils, DayBalanceDTO) {

    function reloadUsersCostPeriods() {
        httpFacade.getResource("usersCostMonths").then(function(response) {
            $scope.costMonths = response.data;
        });
    }
    reloadUsersCostPeriods();

    $scope.monthChanged = function() {
        httpFacade.getResource("usersCashFlowInCostMonth", {monthId: $scope.costMonth})
            .then(function(response) {
                return response.data.map(function(object) {
                    return new DayBalanceDTO(object);
                });
            }).then(function(dayBalances) {
                prepareChartData(dayBalances);
            });
    };

    function prepareChartData(data) {
        $scope.labels = arrayUtils.getSubArrayBySelector(data, function(item) {
            return item.dayInMonth;
        });

        $scope.datasetOverride = {
            label: 'Balance in day [CZK]',
            backgroundColor: "rgba(255,99,132,0.4)"
        };
        $scope.chartData = arrayUtils.getSubArrayBySelector(data, function(item) {
            return item.amount.amount;
        });
    }
}]);