package cz.herain.piggybank.common.mapping.converter;

import cz.herain.piggybank.common.entity.CostYear;
import org.springframework.stereotype.Component;

@Component
public class CostYearToLongIdConverter extends AbstractEntityToIdConverter<CostYear, Long> {
}
