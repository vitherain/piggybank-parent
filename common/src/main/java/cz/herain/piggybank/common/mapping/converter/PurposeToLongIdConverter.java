package cz.herain.piggybank.common.mapping.converter;

import cz.herain.piggybank.common.entity.Purpose;
import org.springframework.stereotype.Component;

@Component
public class PurposeToLongIdConverter extends AbstractEntityToIdConverter<Purpose, Long> {
}
