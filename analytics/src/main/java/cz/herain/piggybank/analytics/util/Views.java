package cz.herain.piggybank.analytics.util;

public class Views {

    public static final String ANALYTICS = "analytics";
    public static final String BALANCES_CHART = "balancesChart";
    public static final String SUMS_BY_PURPOSE_CHART = "sumsByPurposeChart";
    public static final String AVERAGE_BY_PURPOSE_CHART = "averageByPurposeChart";
    public static final String MONTH_CASH_FLOW_CHART = "monthCashFlowChart";
    public static final String PERCENTAGES_BY_PURPOSE_CHART = "percentagesByPurposeChart";
    public static final String AVERAGE_PERCENTAGES_BY_PURPOSE_CHART = "averagePercentagesByPurposeChart";

    private Views() {}
}
