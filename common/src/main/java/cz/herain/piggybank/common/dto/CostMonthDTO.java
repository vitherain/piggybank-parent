package cz.herain.piggybank.common.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static cz.herain.piggybank.common.util.ValidationConstants.MAX_MONTH;
import static cz.herain.piggybank.common.util.ValidationConstants.MIN_MONTH;

public class CostMonthDTO extends BaseDTO {

    @NotNull
    private Long costYearId;

    private int costYearAsNumber;

    private Long pbUserId;

    @Min(value = MIN_MONTH)
    @Max(value = MAX_MONTH)
    private int monthInYear;

    public int getCostYearAsNumber() {
        return costYearAsNumber;
    }

    public void setCostYearAsNumber(int costYearAsNumber) {
        this.costYearAsNumber = costYearAsNumber;
    }

    public Long getCostYearId() {
        return costYearId;
    }

    public void setCostYearId(Long costYearId) {
        this.costYearId = costYearId;
    }

    public int getMonthInYear() {
        return monthInYear;
    }

    public void setMonthInYear(int monthInYear) {
        this.monthInYear = monthInYear;
    }

    public Long getPbUserId() {
        return pbUserId;
    }

    public void setPbUserId(Long pbUserId) {
        this.pbUserId = pbUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CostMonthDTO that = (CostMonthDTO) o;

        if (costYearAsNumber != that.costYearAsNumber) return false;
        if (monthInYear != that.monthInYear) return false;
        if (costYearId != null ? !costYearId.equals(that.costYearId) : that.costYearId != null) return false;
        return pbUserId != null ? pbUserId.equals(that.pbUserId) : that.pbUserId == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (costYearId != null ? costYearId.hashCode() : 0);
        result = 31 * result + costYearAsNumber;
        result = 31 * result + (pbUserId != null ? pbUserId.hashCode() : 0);
        result = 31 * result + monthInYear;
        return result;
    }

    @Override
    public String toString() {
        return "CostMonthDTO{" +
                "id=" + id +
                ", costYearAsNumber=" + costYearAsNumber +
                ", costYearId=" + costYearId +
                ", pbUserId=" + pbUserId +
                ", monthInYear=" + monthInYear +
                '}';
    }
}
