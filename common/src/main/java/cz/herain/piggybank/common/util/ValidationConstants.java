package cz.herain.piggybank.common.util;

public class ValidationConstants {

    public static final int MIN_YEAR = 1970;
    public static final int MIN_MONTH = 1;
    public static final int MAX_MONTH = 12;

    public static final int MIN_AMOUNT = 1;

    public static final int MIN_DAY_IN_MONTH = 1;
    public static final int MAX_DAY_IN_MONTH = 31;

    public static final int STRING_MAX_LENGTH = 255;
}