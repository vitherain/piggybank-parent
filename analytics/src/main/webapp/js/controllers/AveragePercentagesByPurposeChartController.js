angular.module('piggy-bank-analytics').controller("AveragePercentagesByPurposeChartController", ["$scope", "$q", "httpFacade", "toasty", "arrayUtils",
    function($scope, $q, httpFacade, toasty, arrayUtils) {

    function reloadUsersCostPeriods() {
        httpFacade.getResource("usersCostMonths").then(function(response) {
            $scope.costMonths = response.data;
        });
    }
    reloadUsersCostPeriods();

    $scope.monthRangeChanged = function() {
        if ($scope.monthFrom && $scope.monthTo && monthToIsAfterMonthFromOrTheSame()) {
            httpFacade.getResource("usersAveragePercentagesByPurposeInMonth",
                { monthFromId: $scope.monthFrom.id, monthToId: $scope.monthTo.id })
                .then(function(response) {
                    prepareChartData(response.data);
                });
        }
    };

    function monthToIsAfterMonthFromOrTheSame() {
        var yearFrom = $scope.monthFrom.costYearAsNumber;
        var monthFromMonth = $scope.monthFrom.monthInYear;
        var yearTo = $scope.monthTo.costYearAsNumber;
        var monthToMonth = $scope.monthTo.monthInYear;

        return yearTo > yearFrom || (yearTo === yearFrom && monthToMonth >= monthFromMonth);
    }

    function prepareChartData(data) {
        $scope.expensesLabels = arrayUtils.getSubArrayBySelector(data.expenses, function(item) {
            return item.purposeName + ' [%]';
        });

        $scope.incomesLabels = arrayUtils.getSubArrayBySelector(data.incomes, function(item) {
            return item.purposeName + ' [%]';
        });

        $scope.expensesChartData = arrayUtils.getSubArrayBySelector(data.expenses, function(item) {
            return item.percentage;
        });

        $scope.incomesChartData = arrayUtils.getSubArrayBySelector(data.incomes, function(item) {
            return item.percentage;
        });
    }

    $scope.options = {
        legend: {
            display: true,
            position: 'bottom'
        }
    };
}]);