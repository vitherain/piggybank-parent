package cz.herain.piggybank.common.entity;

import javax.persistence.*;

@Entity
@Table(name = "pb_user")
public class PbUser extends BaseEntity {

    @Column(name = "user_id", nullable = false, unique = true)
    private long userId;

    @OneToOne(optional = true)
    @JoinColumn(name = "default_purpose_id", nullable = true)
    private Purpose defaultPurpose;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Purpose getDefaultPurpose() {
        return defaultPurpose;
    }

    public void setDefaultPurpose(Purpose defaultPurpose) {
        this.defaultPurpose = defaultPurpose;
    }

    public boolean hasAsDefaultPurpose(Purpose purpose) {
        return purpose.equals(this.getDefaultPurpose());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PbUser pbUser = (PbUser) o;

        if (userId != pbUser.userId) return false;
        return defaultPurpose != null ? defaultPurpose.equals(pbUser.defaultPurpose) : pbUser.defaultPurpose == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (defaultPurpose != null ? defaultPurpose.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() +
                " PbUser{" +
                "id=" + id +
                ", userId=" + userId +
                ", defaultPurpose=" + (defaultPurpose != null ? defaultPurpose.getId() : null) +
                '}';
    }
}
