angular.module('piggy-bank-analytics').controller("BalancesChartController", ["$scope", "$q", "httpFacade", "toasty", "arrayUtils",
    function($scope, $q, httpFacade, toasty, arrayUtils) {

    function reloadUsersCostPeriods() {
        httpFacade.getResource("usersCostMonths").then(function(response) {
            $scope.costMonths = response.data;
        });
    }
    reloadUsersCostPeriods();

    $scope.monthRangeChanged = function() {
        if ($scope.monthFrom && $scope.monthTo && monthToIsAfterMonthFromOrTheSame()) {
            httpFacade.getResource("usersBalancesBetweenCostMonths",
                { monthFromId: $scope.monthFrom.id, monthToId: $scope.monthTo.id })
                .then(function(response) {
                    prepareChartData(response.data);
                });
        }
    };

    function monthToIsAfterMonthFromOrTheSame() {
        var yearFrom = $scope.monthFrom.costYearAsNumber;
        var monthFromMonth = $scope.monthFrom.monthInYear;
        var yearTo = $scope.monthTo.costYearAsNumber;
        var monthToMonth = $scope.monthTo.monthInYear;

        return yearTo > yearFrom || (yearTo === yearFrom && monthToMonth >= monthFromMonth);
    }

    function prepareChartData(data) {
        $scope.labels = arrayUtils.getSubArrayBySelector(data, function(item) {
            return item.costMonthMonthInYear + '/' + item.costYearAsNumber;
        });

        $scope.datasetOverride = {
            label: 'Balance [CZK]',
            backgroundColor: "rgba(139,214,229,0.7)"
        };
        $scope.chartData = arrayUtils.getSubArrayBySelector(data, function(item) {
            return item.amount.amount;
        });
    }
}]);