package cz.herain.piggybank.common.dto;

/**
 * Created by Vít on 16. 10. 2016.
 */
public class PbUserDTO extends BaseDTO {

    private long userId;
    private Long defaultPurposeId;

    public Long getDefaultPurposeId() {
        return defaultPurposeId;
    }

    public void setDefaultPurposeId(Long defaultPurposeId) {
        this.defaultPurposeId = defaultPurposeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PbUserDTO pbUserDTO = (PbUserDTO) o;

        if (userId != pbUserDTO.userId) return false;
        return defaultPurposeId != null ? defaultPurposeId.equals(pbUserDTO.defaultPurposeId) : pbUserDTO.defaultPurposeId == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (defaultPurposeId != null ? defaultPurposeId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PbUserDTO{" +
                "id=" + id +
                ", defaultPurposeId=" + defaultPurposeId +
                ", userId=" + userId +
                '}';
    }

    public static class PbUserDTOBuilder {
        protected Long id;
        private Long defaultPurposeId;
        private long userId;

        private PbUserDTOBuilder() {
        }

        public static PbUserDTOBuilder aPbUserDTO() {
            return new PbUserDTOBuilder();
        }

        public PbUserDTOBuilder withDefaultPurposeId(Long defaultPurposeId) {
            this.defaultPurposeId = defaultPurposeId;
            return this;
        }

        public PbUserDTOBuilder withUserId(long userId) {
            this.userId = userId;
            return this;
        }

        public PbUserDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public PbUserDTOBuilder but() {
            return aPbUserDTO().withDefaultPurposeId(defaultPurposeId).withUserId(userId).withId(id);
        }

        public PbUserDTO build() {
            PbUserDTO pbUserDTO = new PbUserDTO();
            pbUserDTO.setDefaultPurposeId(defaultPurposeId);
            pbUserDTO.setUserId(userId);
            pbUserDTO.setId(id);
            return pbUserDTO;
        }
    }
}
