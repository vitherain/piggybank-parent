package cz.herain.piggybank.common.mapping.converter;

import cz.herain.piggybank.common.entity.CostMonth;
import org.springframework.stereotype.Component;

@Component
public class CostMonthToLongIdConverter extends AbstractEntityToIdConverter<CostMonth, Long> {
}
