<%@include file="includes/header.jsp" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<div class="container-fluid">
    <%--MENU--%>
    <tag:menu/>
    <%--END MENU--%>
    <c:if test="${pbUser != null}">
        <h2>Piggy bank analytics portlet</h2>

        <p>This portlet offers the possibility to analyze your personal financial data.
            You can choose from various charts.</p>

        <p><strong>Balances chart</strong> shows bar chart with month balances between specified cost months.</p>

        <p><strong>Sum by purpose chart</strong> shows bar chart which sums incomes and expenses by purpose in the
            specified cost month.</p>

        <p>On the <strong>Average by purposes chart</strong> you can see the averages of incomes and expenses by purpose
            between specified cost months.</p>

        <p><strong>Month cash flow chart</strong> shows day-based cash flow in the cost month you select.</p>

        <p><strong>Percentages by purpose chart</strong> is a pie chart which shows the percentage distribution of incomes
            which comes from the specific purpose and expenses spent on the specific purpose in the specified cost month.</p>

        <p>The <strong>Average percentages by purpose chart</strong> is similar, but shows average data in the range of
            specified cost months.</p>
    </c:if>

    <c:if test="${pbUser == null}">
        <h1>Please login to portal</h1>
    </c:if>
</div>

<%@include file="includes/footer.jsp" %>