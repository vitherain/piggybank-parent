package cz.herain.piggybank.common.dto;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by Vit Herain on 26/10/2016.
 */
public class AmountDTO implements Serializable {

    private BigInteger amount;

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AmountDTO amountDTO = (AmountDTO) o;

        return amount != null ? amount.equals(amountDTO.amount) : amountDTO.amount == null;

    }

    @Override
    public int hashCode() {
        return amount != null ? amount.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "AmountDTO{" +
                "amount=" + amount +
                '}';
    }

    public static class AmountDTOBuilder {
        private BigInteger amount;

        private AmountDTOBuilder() {
        }

        public static AmountDTOBuilder anAmountDTO() {
            return new AmountDTOBuilder();
        }

        public AmountDTOBuilder withAmount(BigInteger amount) {
            this.amount = amount;
            return this;
        }

        public AmountDTOBuilder but() {
            return anAmountDTO().withAmount(amount);
        }

        public AmountDTO build() {
            AmountDTO amountDTO = new AmountDTO();
            amountDTO.setAmount(amount);
            return amountDTO;
        }
    }
}
