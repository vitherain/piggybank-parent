package cz.herain.piggybank.common.entity;

import javax.persistence.*;

@Entity
@Table(name = "purpose")
public class Purpose extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean active;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private PbUser pbUser;

    public Purpose() {
    }

    public Purpose(final Long id, final String name) {
        super(id);
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PbUser getPbUser() {
        return pbUser;
    }

    public void setPbUser(PbUser pbUser) {
        this.pbUser = pbUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Purpose purpose = (Purpose) o;

        if (active != purpose.active) return false;
        return name != null ? name.equals(purpose.name) : purpose.name == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() +
                " Purpose{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", pbUserId=" + (pbUser != null ? pbUser.getId() : null) +
                '}';
    }
}
