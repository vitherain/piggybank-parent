package cz.herain.piggybank.common.service;


import cz.herain.piggybank.common.dto.AmountDTO;
import cz.herain.piggybank.common.dto.IncomeDTO;
import cz.herain.piggybank.common.service.base.*;

import java.util.List;

public interface IncomeService extends BaseGetAllService<IncomeDTO>, BaseGetOneService<IncomeDTO>,
        BaseSaveOrUpdateOneService<IncomeDTO>, BaseSaveSeveralService<IncomeDTO>,
        BaseDeleteService {

    List<IncomeDTO> getByMonthIdOrderedByDayAsc(long monthId);

    AmountDTO getSumOfIncomesByMonthId(long monthId);

    List<AmountDTO> getAveragesByPurposeAndCostMonthsOrderedByPurposeName(List<Long> costMonthIds, long userId);

    AmountDTO getSumOfIncomesByPurposeIdAndMonthId(long purposeId, long monthId);

    AmountDTO getSumOfIncomesByPurposeIdAndMonthIds(long purposeId, List<Long> costMonthIds);

    AmountDTO getSumOfIncomesByMonthIds(List<Long> costMonthIds);
}
