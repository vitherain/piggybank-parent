angular.module('piggy-bank-analytics').controller("PercentagesByPurposeChartController", ["$scope", "$q", "httpFacade", "toasty", "arrayUtils",
    function($scope, $q, httpFacade, toasty, arrayUtils) {

    function reloadUsersCostPeriods() {
        httpFacade.getResource("usersCostMonths").then(function(response) {
            $scope.costMonths = response.data;
        });
    }
    reloadUsersCostPeriods();

    $scope.monthChanged = function() {
        httpFacade.getResource("usersPercentagesByPurposeInMonth", {monthId: $scope.costMonth})
            .then(function(response) {
                prepareChartData(response.data);
            });
    };

    function prepareChartData(data) {
        $scope.expensesLabels = arrayUtils.getSubArrayBySelector(data.expenses, function(item) {
            return item.purposeName + ' [%]';
        });

        $scope.incomesLabels = arrayUtils.getSubArrayBySelector(data.incomes, function(item) {
            return item.purposeName + ' [%]';
        });

        $scope.expensesChartData = arrayUtils.getSubArrayBySelector(data.expenses, function(item) {
            return item.percentage;
        });

        $scope.incomesChartData = arrayUtils.getSubArrayBySelector(data.incomes, function(item) {
            return item.percentage;
        });
    }

    $scope.options = {
        legend: {
            display: true,
            position: 'bottom'
        }
    };
}]);