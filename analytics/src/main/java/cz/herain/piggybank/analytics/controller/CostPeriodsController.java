package cz.herain.piggybank.analytics.controller;

import com.google.gson.Gson;
import cz.herain.piggybank.common.controller.PortletControllerAdvice;
import cz.herain.piggybank.common.dto.CostMonthDTO;
import cz.herain.piggybank.common.service.CostMonthService;
import cz.herain.piggybank.common.util.ContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class CostPeriodsController extends PortletControllerAdvice {

    @Autowired
    private ContextUtil contextUtil;

    @Autowired
    private CostMonthService costMonthService;

    @ResourceMapping("usersCostMonths")
    public void getUsersCostMonths(ResourceRequest request, ResourceResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();

        long userId = contextUtil.getCurrentPbUserId();

        List<CostMonthDTO> costMonths = costMonthService.getAllSortedByYearAndMonthAsc(userId);
        String costMonthsJson = gson.toJson(costMonths);

        out.print(costMonthsJson);
    }
}
